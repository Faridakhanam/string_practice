<?php
	// Returns a string with the first character of str capitalized, if that character is alphabetic. 
	// string ucfirst ( string $str )
	
	$foo = 'hello world!';
	$foo = ucfirst($foo);             // Hello world!
	echo $foo;
	echo "<br>";
	$bar = 'HELLO WORLD!';
	$bar = ucfirst($bar);             // HELLO WORLD!
	echo $bar;
	echo "<br>";
	$bar = ucfirst(strtolower($bar));
	echo $bar;
	echo "<br>";
?>
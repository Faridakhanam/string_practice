<?php
	// Pad a string to a certain length with another string 
	// string str_pad ( string $input , int $pad_length [, string $pad_string = " " [, int $pad_type = STR_PAD_RIGHT ]] )

	$input = "Alien";
	echo str_pad($input, 10);                      
	echo str_pad($input, 10);                      
	echo str_pad($input, 10);                      
	echo str_pad($input, 10, "-=", STR_PAD_LEFT);  
	echo str_pad($input, 10, "_", STR_PAD_BOTH);   
	echo str_pad($input,  6, "___");               
     
?>
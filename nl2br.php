<?php
	// Returns string with '<br />' or '<br>' inserted before all newlines (\r\n, \n\r, \n and \r). 
	// string nl2br ( string $string [, bool $is_xhtml = true ] )
	
	echo nl2br("foo isn't\n bar");
	echo "<br>";
	$string = "This\r\nis\n\ra\nstring\r";
	echo nl2br($string);
	
?>
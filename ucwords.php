<?php
	// Returns a string with the first character of each word in str capitalized, if that character is alphabetic. 
	// string ucwords ( string $str [, string $delimiters = " \t\r\n\f\v" ] )
	
	$foo = 'hello world!';
	$foo = ucwords($foo);             // Hello World!
	echo $foo;
	echo "<br>";

	$bar = 'HELLO WORLD!';
	$bar = ucwords($bar);             // HELLO WORLD!
	echo $bar;
	echo "<br>";
	$bar = ucwords(strtolower($bar)); // Hello World!
	echo $bar;
	echo "<br>";
?>


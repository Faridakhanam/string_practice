<?php
	// Join array elements with a glue string. 
	// string implode ( string $glue , array $pieces )

	$array = array('lastname', 'email', 'phone');
	$comma_separated = implode(",", $array);

	echo $comma_separated; // lastname,email,phone

	// Empty string when using an empty array:
	var_dump(implode('hello', array())); // string(0) ""


?>
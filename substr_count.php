<?php
	// substr_count() returns the number of times the needle substring occurs in the haystack string.
	// int substr_count ( string $haystack , string $needle [, int $offset = 0 [, int $length ]] )
	
	$text = 'This is a test';
	echo strlen($text); // 14
	echo "<br>";
	echo substr_count($text, 'is'); // 2
	echo "<br>";
	// the string is reduced to 's is a test', so it prints 1
	echo substr_count($text, 'is', 3);
	echo "<br>";
	// the text is reduced to 's i', so it prints 0
	echo substr_count($text, 'is', 3, 3);
	echo "<br>";
	// generates a warning because 5+10 > 14
	echo substr_count($text, 'is', 5, 10);

?>

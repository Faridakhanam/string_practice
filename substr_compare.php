<?php
	// substr_compare() compares main_str from position offset with str up to length characters. 
	// int substr_compare ( string $main_str , string $str , int $offset [, int $length [, bool $case_insensitivity = false ]] )

	echo substr_compare("abcde", "BC", 1, 2, true); // 0
		echo "<br>";
	echo substr_compare("abcde", "bc", 1, 3); // 1
		echo "<br>";
	echo substr_compare("abcde", "cd", 1, 2); // -1
		echo "<br>";
	echo substr_compare("abcde", "abc", 5, 1); 
?>
<?php
	// This function returns a string with whitespace stripped from the beginning and end of str. 
	// string trim ( string $str [, string $character_mask = " \t\n\r\0\x0B" ] )
	
	$text   = "\t\tThese are a few words :) ...  ";
	$binary = "\x09Example string\x0A";
	$hello  = "Hello World";
	var_dump($text, $binary, $hello);

	print "\n";

	$trimmed = trim($text);
	var_dump($trimmed);

	$trimmed = trim($text, " \t.");
	var_dump($trimmed);

	$trimmed = trim($hello, "Hdle");
	var_dump($trimmed);

	$trimmed = trim($hello, 'HdWr');
	var_dump($trimmed);

	// trim the ASCII control characters at the beginning and end of $binary
	// (from 0 to 31 inclusive)
	$clean = trim($binary, "\x00..\x1F");
	var_dump($clean);
?>
<?php
	//Returns an array of strings, each of which is a substring of string formed by splitting it on boundaries formed by the string delimiter. 

	// array explode ( string $delimiter , string $string [, int $limit ] )
	
	
	// Example 1
	$pizza  = "piece1 piece2 piece3 piece4 piece5 piece6";
	$pieces = explode(" ", $pizza);
	echo $pieces[0]; // piece1
	echo $pieces[1]; // piece2

?>

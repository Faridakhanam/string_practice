<?php
	// Returns a string with backslashes before characters that need to be escaped
	// string addslashes ( string $str )
	
	
	$str = "Is your name O'Reilly?";

	// Outputs: Is your name O\'Reilly?
	echo addslashes($str);
?>

